const express = require("express");
const app = express();

const hbs = require('hbs')

const port = process.env.PORT || 4000

//app.use(express.static(__dirname + '/public'))

//Express HBS Engine
hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs');

app.get("/", function (req, res){
    /* res.render('home', {
        nombre: 'George',
        anio: new Date().getFullYear()
    }) */
    res.render('home')
});

app.get("/decoraciones", function(req, res){
    res.render('decoraciones')
});

app.get("/contactanos", function(req, res){
    res.render('contactanos')
});

app.get("/articulosPrevencion", function(req, res){
    res.render('articulosPrevencion')
});
app.listen(port, ()=> console.log(`Escuchando peticiones en puerto ${port}`));
